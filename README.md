# Coding Style Capacitat

Nós da [Capacitat](http://www.capacitat.com.br), visando um melhor entrosamento entre os desenvolvedores, estamos disponibilizando um *guide line* de *coding style*, que deverá ser seguido em todos os projetos desenvolvidos por aqui.  
Esse guia aborda desde a padronização de *front end* até o fluxo ideial de desenvolvimento do *back end* e utilização ideal, interna, do GIT.

## Padronização Básica

### Copdificação (Encoding)
O *encoding* padrão utilizado na capacitat é o **UTF-8**, e os seguintes itens devem ser observados e configurados para utilizar esse *encoding*:

* Arquivos, tanto ao escrever quanto ao salvar;
* Bancos de dados;

Para projetos em Ruby 1.9 ou superior, é imprescindível a utilização da seguinte marcação na primeira linha de todos os arquivos que contenham carcteres especiais:

	# encoding: utf-8

 
### Nomenclatura

Todos os nomes criados durante o desenvolvimento, seja uma variável, uma classe, um arquivo, tabelas e colunas de banco de dados ou qualquer outro tipo de nome que será, de algum modo, reutilizado por outro desenvolvedor, deve obrigatóriamente ser criado em **inglês**.  
É importante criar nomes descritivos o suficiente para que nenhum outro desenvolvedor precise consultar outro arquivo, ou mesmo a declaração de variável/função.  
Para **nomes compostos** cada linguagem deve seguir uma padronização própria.

* **HTML** - Classes - Devem ser separados sempre por hífem (`-`) e manter todas as letras em caixa baixa. Ex: `<div class="test-name">`;
* **HTML** - IDs - Devem ser separados por *underlines*  (`_`) e manter todas as letras em caixa baixa. Ex: `<div id="test_name">`;
* **Javascript** - Variáveis - Devem ser separados por *underlines* (`_`), e manter todas as letras em caixa baixa. Ex: `var test_name = "";`;
* **Javascript** - Funções - Devem ser escritos em *camel case* com a primeira letra sempre caixa baixa. Ex: `function testName();`;
* **Javascript** - Classes - Devem ser escritos em *camel case* com a primeira letra sempre caixa alta. Ex: `class TestName{}`;
* **PHP** - Variáveis - Devem ser separados por *underline* (`_`) e mater todas as letras em caixa baixa. Ex: `$test_name;`;
* **PHP** - Variáveis Globais - Devem ser separados por *underline* (`_`) e mater todas as letras em caixa alta. Ex: `define(TEST_NAME, "");`;
* **PHP** - Funções - Devem ser separados por *underline* (`_`) e manter todas as letras em caixa baixa. Ex: `function test_name(){};`;
* **PHP** - Classes - Devem ser separados por *underline* (`_`) e mater **somente** a primeira letra em caixa alta. Ex: `class Test_name{}`;

## Front End

A semantica deve ser a prioridade em nossos códigos de *front end* e por isso os novos projetos devem usar todas as *tags* suportadas atualmente do HTML 5, tais como: `<header>`, `<footer>`, `<section>`, `<nav>` e etc.  

Sempre dar preferencia à classes ao invés de ids nos elementos HTML, a menos que esse elemento seja capturado por javascript.  

**Ruim:**  

	<div id="name"></div>
	
**Bom:**  

	<div class="name"></div>
	
**Bom:**  

	<div id="name"></div>
	<script type="text/javascript">
		alert($("#name").text());
	</script>

## Back End

Nessa seção vamos abordar alguns padrões de código que devem ser adotados também aos códigos **javascript**.

### Style guide para Ruby

Todo o estilo para escrever códigos Ruby devem seguir o *style guide* do Github que pode ser consultado [aqui](https://github.com/styleguide/ruby/).


### Comentários

O comentário é a forma de expressão e comunicação entre os desenvolvedores de um projeto. É muito importante comentar todos os trechos de código que lhe pareçam confusos ou de dificil entendimento.

Todas as funções e classes devem ter ter um um bloco de documentação logo acima de sua assinatura.

Classes:

	/**
	 * Super Class	  
	 * 
	 * @package Package Name 
	 * @subpackage Subpackage 
	 * @category Category 
	 * @author Author Name 
	 * @link http://example.com 
	 */
	class Super_class {}
Funções:

	/**
	 * Encodes string for use in XML	  
	 * 
	 * @author Author Name
	 * @access public 
	 * @param string 
	 * @return string 
	 */ 
	function xml_encode($str)
	
Os itens listados da documentação são opcionais, mas quanto mais informações forem fornecidas melhor.

### TRUE, FALSE e NULL

Em **PHP** é importante manter essas palavras em caixa alta.

**Exemplo:**

	if($test == TRUE){};
	function foo($bar = NULL){};

### Comparações

É importante ter certeza dos valores que você está comparando, procure sempre utilizar o `===` e o `!==` ao invés do `==` e `!=`.  

### Uma classe por arquivo

Sempre mantenha apenas uma única classe para arquivo em seu projeto.

### Quebras de linha

Os arquivos devem ser salvos com quebras de linha Unix. Este é mais um problema para os desenvolvedores que trabalham no Windows, mas em qualquer caso, basta garantir que o seu editor de texto está configurado para salvar os arquivos com as quebras de linha Unix.

### Indentação

É importante manter a indentação padronizada durante todo o projeto. Na capacitat nós temos duas formas distintas de indentação que devem ser seguidas de acordo com a linguagem padrão do projeto. Para **PHP** utilizamos uma tabulação (equivalente a quatro espaços) e para **Ruby** utilizamos dois espaços (*soft tab*).  
**Importante**: Esse item pode ser discutido entre os desenvolvedores para decidir se os projetos em **PHP** devem continuar utilzando a tabulação ou devem começar a utilizar o *soft tab* de dois espaços.

Para declaração de funções e classes, devemos manter a primeira chave na mesma linha da assinatura da função/classe.

**Bom:**

	class Test{
		// ...
	}
	
	function test(){
		// ...
	}
	
**Ruim:**

	class Test
	{
		//...
	}
	
	function test()
	{
		//...
	}
	
### PHP - Short open tag

Sempre utilize as tags completas do PHP, indenpendente do servidor aceitar ou não o short_open_tag.

**Ruim:**

	<? echo $foo; ?>
	<?= $foo; ?>
	
**Bom:**

	<?php echo $foo; ?>
	
### Uma declaração por linha

Sempre declare suas variáveis linha a linha.

**Ruim:**

	$foo = 'teste'; $bar = 'teste2'; $bat = array($foo,$bar);
	
**Bom:**

	$foo = 'teste'; 
	$bar = 'teste2'; 
	$bat = array($foo,$bar);
	
### Strings

Sempre use strings de aspas simples, a menos que você precise variáveis e se precisar utilze chaves para envolver essas variáveis. Você também pode usar strings delimitadas por aspas se a string contém aspas simples, então você não precisa usar caracteres de escape.

**Ruim:**

	"My String"
	"My string $foo"
	'SELECT foo FROM bar WHERE baz = \'bag\''
	
**Bom:**

	'My String'
	"My string {$foo}"
	"SELECT foo FROM bar WHERE baz = 'bag'"
	
## Para todo o restante
Siga o bom senso, escolha formas elegantes de resolver seus problemas, sempre pensando na próxima pessoa que irá utilizar seu código.
	